![version 8.0.0](https://img.shields.io/badge/version-0.0.7-green.svg?style=flat-square)
   ![boilerplate boilerplate.0.0.1](https://img.shields.io/badge/boilerplate-boilerplate.0.0.1-lightgray.svg?style=flat-square)
   ![docker boilerplate.0.0.1](https://img.shields.io/badge/docker-boilerplate.0.0.1-lightgray.svg?style=flat-square)
   ![installer boilerplate.0.0.1](https://img.shields.io/badge/installer-boilerplate.0.0.1-lightgray.svg?style=flat-square)
   ![license MIT](https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square)

   project:

    - name:
    - manager:
    - phpversion: PHP7

   repositories:

    - hive_thm:
    - hive_docker:
    - hive_installer:
    - hive_boilerplate:


   ```
   #!bash

   NAME
       hive_installer

   SYNOPSIS
       make install

   DESCRIPTION
       The Installer installs TYPO3 with composer.
       In addition, a dummy database is imported and the default extentsions are installed.

       Note: A configuration via browser is necessary for the installation.

   REQUIREMENTS
       OS X 10.10.3 Yosemite or newer
       At least 4GB of RAM
       VirtualBox prior to version 4.3.30 must NOT be installed (it is incompatible with Docker for Mac). Docker for Mac will error out on install in this case. Uninstall the older version of VirtualBox and re-try the install.
       Git
       Composer
       Docker for Mac
       SourceTree
       Bitbucket teufels-teammember (deploy)
       SSH key authorisation for our servers (deploy)
       hive_creator (maked project)

       See also: https://bitbucket.org/account/user/teufels/projects/DOC.

       Using the make command requires configuration of the corresponding 'install.ini' and in the 'hive_docker' folder must be execute 'make docker plain' before.

       See also: https://bitbucket.org/teufels/hive_creator

   ERRORCODES:
       [g3978p78jzk312a0el4v1nd4ut6tb3z8]
       [dz67w45uqa1562czv25423r46w99k2te]
       [fsf9qxy3i3pi4k4usi18umx21r9q837v]
       [kk6vegv83o41kki30nzexyd6y6w5wuwy]
       [x6eyawow2ni9tv946tzi18099283d6gr]

   LISENCE:
       The MIT License (MIT)

       Copyright (c) 2017
       Andreas Hafner <a.hafner@teufels.com>,
       Dominik Hilser <d.hilser@teufels.com>,
       Georg Kathan <g.kathan@teufels.com>,
       Hendrik Krüger <h.krueger@teufels.com>,
       Josymar Escalona Rodriguez <j.rodriguez@teufels.com>,
       Perrin Ennen <p.ennen@teufels.com>,
       Timo Bittner <t.bittner@teufels.com>,
       teufels GmbH <digital@teufels.com>

       Permission is hereby granted, free of charge, to any person obtaining a copy
       of this software and associated documentation files (the "Software"), to deal
       in the Software without restriction, including without limitation the rights
       to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
       copies of the Software, and to permit persons to whom the Software is
       furnished to do so, subject to the following conditions:

       The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.

       THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.

   ```