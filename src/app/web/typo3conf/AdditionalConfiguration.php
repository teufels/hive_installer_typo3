<?php
/**
 * The MIT License (MIT)
 *
 *
 * Copyright (c) 2016
 * Andreas Hafner <a.hafner@teufels.com>,
 * Dominik Hilser <d.hilser@teufels.com>,
 * Georg Kathan <g.kathan@teufels.com>,
 * Hendrik Krüger <h.krueger@teufels.com>,
 * Perrin Ennen <p.ennen@teufels.com>,
 * Timo Bittner <t.bittner@teufels.com>,
 * teufels GmbH <digital@teufels.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

/**
 *  Development environment
 */
if (strpos($_SERVER["HTTP_HOST"] , "development." ) !== false) {
    $config;
    if (strpos($_SERVER["HTTP_HOST"] , "development.localhost" ) !== false) {
        $config = parse_ini_file('local.ini');
    } else {
        $config = parse_ini_file('config/development.ini');
    }

    $db = [
        'charset' => $config['charset'],
        'dbname' => $config['dbname'],
        'driver' => $config['driver'],
        'host' => $config['host'],
        'password' => $config['password'],
        'port' => $config['port'],
        'user' => $config['user'],
    ];

    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] = $db;

    if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)){
        $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '0';
    } else {
        $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '1';
    }
    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '*';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = 'file';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['exceptionalErrors'] = '20480';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['debug'] = '1';

    //    $GLOBALS['TYPO3_CONF_VARS']['SYS'] = array(
    //       'displayErrors' => '1',
    //        'devIPmask' => '*',
    //        'errorHandler' => 'TYPO3\\CMS\\Core\\Error\\ErrorHandler',
    //        'errorHandlerErrors' => 28674,//E_ALL ^ E_NOTICE ^ E_WARNING ^ E_USER_ERROR ^ E_USER_NOTICE ^ E_USER_WARNING ^ E_RECOVERABLE_ERROR, //^ E_RECOVERABLE_ERROR,
    //        'exceptionalErrors' => 28674,//E_ALL ^ E_NOTICE ^ E_WARNING ^ E_USER_ERROR ^ E_USER_NOTICE ^ E_USER_WARNING ^ E_RECOVERABLE_ERROR,
    //        'debugExceptionHandler' => 'TYPO3\\CMS\\Core\\Error\\DebugExceptionHandler',
    ////        'productionExceptionHandler' => 'TYPO3\\CMS\\Core\\Error\\DebugExceptionHandler',
    ////        'systemLogLevel' => '0',
    //////        'systemLog' => 'mail,test@localhost.local,4;error_log,,2;syslog,LOCAL0,,3;file,/abs/path/to/logfile.log',
    //        'enable_errorDLOG' => '1',
    //        'enable_exceptionDLOG' => '1',
    //    );

    $GLOBALS['TYPO3_CONF_VARS']['BE']['compressionLevel'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['compressionLevel'] = '9';
}

/**
 * Staging environment
 */
else if (strpos($_SERVER["HTTP_HOST"] , "staging." ) !== false) {
    $config = parse_ini_file('config/staging.ini');

    $db = [
        'charset' => $config['charset'],
        'dbname' => $config['dbname'],
        'driver' => $config['driver'],
        'host' => $config['host'],
        'password' => $config['password'],
        'port' => $config['port'],
        'user' => $config['user'],
    ];

    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] = $db;

    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = '2';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['debug'] = '1';

    $GLOBALS['TYPO3_CONF_VARS']['BE']['compressionLevel'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['compressionLevel'] = '9';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfiguration']['extbase_object']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';

    /**
     * Production environment
     */
} else {
    $config = parse_ini_file('config/production.ini');

    $db = [
        'charset' => $config['charset'],
        'dbname' => $config['dbname'],
        'driver' => $config['driver'],
        'host' => $config['host'],
        'password' => $config['password'],
        'port' => $config['port'],
        'user' => $config['user'],
    ];

    $GLOBALS['TYPO3_CONF_VARS']['DB']['Connections']['Default'] = $db;

    $GLOBALS['TYPO3_CONF_VARS']['BE']['debug'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['debug'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['displayErrors'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['enableDeprecationLog'] = '';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['sqlDebug'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['systemLogLevel'] = '2';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['clearCacheSystem'] = '1';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['debug'] = '1';

    $GLOBALS['TYPO3_CONF_VARS']['BE']['compressionLevel'] = '0';
    $GLOBALS['TYPO3_CONF_VARS']['FE']['compressionLevel'] = '9';
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfiguration']['extbase_object']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
}

/* $GLOBALS['TYPO3_CONF_VARS']['SYS']['useCachingFramework'] = '1';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pages']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\MemcachedBackend';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pages']['options'] = array('servers' => array('localhost:11211'),);
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pagesection']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\MemcachedBackend';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_pagesection']['options'] = array('servers' => array('localhost:11211'),);
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_hash']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\MemcachedBackend';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['cache_hash']['options'] = array('servers' => array('localhost:11211'),);
*
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_reflection']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\MemcachedBackend';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_reflection']['options'] = array('servers' => array('localhost:11211'),);
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_object']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\MemcachedBackend';
* $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['extbase_object']['options'] = array('servers' => array('localhost:11211'),);
*/

/*
 * Usage
 * $logger = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Log\LogManager')->getLogger(__CLASS__);
 * $logger->log($level, $message, $data);
 * https://docs.typo3.org/typo3cms/CoreApiReference/ApiOverview/Logging/Logger/Index.html
 */

$GLOBALS['TYPO3_CONF_VARS']['LOG']['writerConfiguration'] = array(
    // configuration for EMERGENCY level log entries
    \TYPO3\CMS\Core\Log\LogLevel::EMERGENCY => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_EMERGENCY.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::ALERT => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_ALERT.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::CRITICAL => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_CRITICAL.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::ERROR => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_error.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::WARNING => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_WARNING.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::NOTICE => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_NOTICE.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::INFO => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_INFO.log'
        )
    ),
    \TYPO3\CMS\Core\Log\LogLevel::DEBUG => array(
        // add a FileWriter
        'TYPO3\\CMS\\Core\\Log\\Writer\\FileWriter' => array(
            // configuration for the writer
            'logFile' => 'typo3temp/logs/typo3_DEBUG.log'
        )
    )
);

/*
 * extend addRootLineFields
 */
$aExtend = array("description","keywords","author");
$sAddRootLineFields = &$GLOBALS["TYPO3_CONF_VARS"]["FE"]["addRootLineFields"];
$aAddRootLineFields = ($sAddRootLineFields == '' ? array() : (strpos($sAddRootLineFields, "," ) !== false ? explode(",", rtrim($sAddRootLineFields, ',')) : array(0 => $sAddRootLineFields)));
foreach($aExtend as $sExtend) {
    if(!in_array($sExtend,$aAddRootLineFields)) {
        $aAddRootLineFields[] = $sExtend;
    }
}
$sAddRootLineFields = implode(",",$aAddRootLineFields);

function user_t2configIsMobile() {

    $bMobile = false;
    $useragent=$_SERVER['HTTP_USER_AGENT'];
    if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|ipad|ipod|bada|android|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) $bMobile = true;

    return $bMobile;
}

/*
 *
 * 503 Maintenance Handling
 *
 * => Changes $GLOBALS['TYPO3_CONF_VARS']['SYS']['devIPmask'] to '37.247.80.136,37.200.100.39,134.119.41.40';
 *
 */
//if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/typo3conf/teufels_thm_error/Ressources/Puplic/503/503.php")) {
//    require_once("/typo3conf/teufels_thm_error/Ressources/Puplic/503/503.php");
//}


/*
 *
 * 404 Error Handling
 *
 */
if (file_exists($_SERVER['DOCUMENT_ROOT'] . "/typo3conf/teufels_thm_error/Ressources/Puplic/404/404.php")) {
    require_once("/typo3conf/teufels_thm_error/Ressources/Puplic/404/404.php");
}