#!/bin/bash

#######################################
## install extensions
#######################################

initInstallExtensions() {
    logMsg "start install extensions"

    logMsg "install extension_builder"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install extension_builder

    logMsg "install vhs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install vhs

    logMsg "install hive_cfg_page"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cfg_page
    logMsg "install hive_cfg_typoscript"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cfg_typoscript
    logMsg "install hive_cfg_user"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cfg_user
    logMsg "install hive_cpt_brand"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_brand

    logMsg "install hive_cpt_cnt_bs_btn"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_cnt_bs_btn
    logMsg "install hive_cpt_cnt_bs_carousel"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_cnt_bs_carousel
    logMsg "install hive_cpt_cnt_bs_tab_collapse"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_cnt_bs_tab_collapse
    logMsg "install hive_cpt_cnt_img"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_cnt_img

    logMsg "install hive_cpt_dynamiccontent"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_dynamiccontent
    logMsg "install hive_cpt_nav_anchor"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_anchor
    logMsg "install hive_cpt_nav_breadcrumb"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_breadcrumb
    logMsg "install hive_cpt_nav_mega"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_mega
    logMsg "install hive_cpt_nav_mobile"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_mobile
    logMsg "install hive_cpt_nav_simple"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_simple
    #logMsg "install hive_cpt_nav_simple_dd"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_simple_dd
    #logMsg "install hive_cpt_nav_special"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_special
    #logMsg "install hive_cpt_nav_sub"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_cpt_nav_sub

    logMsg "install hive_ovr_fluidstyledcontent"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_ovr_fluidstyledcontent
    logMsg "install metaseo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install metaseo
    logMsg "install filemetadata"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install filemetadata
    logMsg "install scheduler"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install scheduler
    logMsg "install realurl"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install realurl
    logMsg "install hive_ovr_realurl"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_ovr_realurl
    logMsg "install powermail"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install powermail
    logMsg "install news"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install news
    logMsg "install hive_ovr_metaseo"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_ovr_metaseo
    logMsg "install sourceopt"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install sourceopt
    logMsg "install hive_ovr_sourceopt"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_ovr_sourceopt

    logMsg "install hive_thm_less"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_less
    logMsg "install hive_thm_backendlayout"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_backendlayout
    logMsg "install hive_thm_blazy"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_blazy
    logMsg "install hive_thm_bs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_bs
    #logMsg "install hive_thm_bs_toolkit"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_bs_toolkit
    logMsg "install hive_thm_custom"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_custom
    logMsg "install hive_thm_ie_dinosaurs"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_ie_dinosaurs
    logMsg "install hive_thm_jq"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_jq
    #logMsg "install hive_thm_jq_anima"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_jq_anima
    logMsg "install hive_thm_jq_focuspoint"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_jq_focuspoint
    logMsg "install hive_thm_jq_hammer"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_jq_hammer
    #logMsg "install hive_thm_jq_transit"
    #php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_jq_transit
    logMsg "install hive_thm_modernizr"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_modernizr
    logMsg "install hive_thm_pace"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_pace
    logMsg "install hive_thm_webfontloader"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_thm_webfontloader

    logMsg "install hive_viewhelpers"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_viewhelpers
    logMsg "install hive_hooks"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_hooks
    logMsg "install hive_honeypot"
    php web/typo3/cli_dispatch.phpsh  extbase extension:install hive_honeypot

    logMsg "finish install extensions"
}

logMsg() {
    echo -e "" \
    "$*"
}

initInstallExtensions