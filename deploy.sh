#!/bin/bash

###########################################
## Deploy local to development for TYPO3 ##
###########################################

version="v.0.0.7";

if [ ! -f "install.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <install.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

if [ ! -f "install.local.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <install.local.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

if [ ! -f "src/development.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <development.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi


":: development_database: $development_database\n" \
    ":: development_extTablesDefinitionScript: $development_extTablesDefinitionScript\n" \
    ":: development_host: $development_host\n" \
    ":: development_password: $development_password\n" \
    ":: development_port: $development_port\n" \
    ":: development_username: $development_username\n" \
    "::\n" \
    ":: staging_database: $staging_database\n" \
    ":: staging_extTablesDefinitionScript: $staging_extTablesDefinitionScript\n" \
    ":: staging_host: $staging_host\n" \
    ":: staging_password: $staging_password\n" \
    ":: staging_port: $staging_port\n" \
    ":: staging_username: $staging_username\n" \
    "::\n" \
    ":: production1_database: $production1_database\n" \
    ":: production1_extTablesDefinitionScript: $production1_extTablesDefinitionScript\n" \
    ":: production1_host: $production1_host\n" \
    ":: production1_password: $production1_password\n" \
    ":: production1_port: $production1_port\n" \
    ":: production1_username: $production1_username\n" \
    "::\n" \
    ":: production2_database: $production2_database\n" \
    ":: production2_extTablesDefinitionScript: $production2_extTablesDefinitionScript\n" \
    ":: production2_host: $production2_host\n" \
    ":: production2_password: $production2_password\n" \
    ":: production2_port: $production2_port\n" \
    ":: production2_username: $production2_username\n" \
    "::\n" \
    ":: hotfix_database: $hotfix_database\n" \
    ":: hotfix_extTablesDefinitionScript: $hotfix_extTablesDefinitionScript\n" \
    ":: hotfix_host: $hotfix_host\n" \
    ":: hotfix_password: $hotfix_password\n" \
    ":: hotfix_port: $hotfix_port\n" \
    ":: hotfix_username: $hotfix_username\n" \


if [ ! -f "src/staging.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <staging.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

if [ ! -f "src/production1.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <production1.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

if [ ! -f "src/staging.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <staging.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

if [ ! -f "src/hotfix.ini" ]; then
    errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
    ":: File <hotfix.ini> does not exist\n" \
    ":: Feel free to read the manual by executing 'make help'"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        exitMsg
        exit
    fi
fi

#include config to create a new project
. install.ini
. install.local.ini
. src/development.ini

development_database="$database"
development_host="$host"
development_password="$password"
development_port="$port"
development_username="$username"

. src/staging.ini

staging_database="$database"
staging_host="$host"
staging_password="$password"
staging_port="$port"
staging_username="$username"

. src/production.ini

production_database="$database"
production_host="$host"
production_password="$password"
production_port="$port"
production_username="$username"

. src/production1.ini

production1_database="$database"
production1_host="$host"
production1_password="$password"
production1_port="$port"
production1_username="$username"

. src/hotfix.ini

hotfix_database="$database"
hotfix_host="$host"
hotfix_password="$password"
hotfix_port="$port"
hotfix_username="$username"

#mapping
boilerplatename=$hive_boilerplate
sshUser=$sshuser
remoteFolder=$remotefolderpath
repoInstaller=$repoinstaller
repoDeploy=$repodeploy

startCounter=$(date +%s)
trashTime=$(date +"%Y-%m-%d_%H-%M-%S")

init() {
    clear
    infoMsg "$DRY"
    showVariables

    read -rsp $'\n\nPress any key to continue...\n' -n1 key

    checkPlaceholderVariables

    infoMsg "::\n" \
    ":: initializing development TYPO3\n" \
    "::\n"

    setupRemoteFolders

    uploadConfigDatabases

    exportLocalDatabaseToDevelopment

    uploadLocalProjectToDevelopment

    #handleDatabaseForInit

    setPermissions

    #TODO: check .htaccess
    logMsg "TODO: check .htaccess"
    #logMsg "rename .htaccess"
    #ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/_.htaccess ${remoteFolder}/releases/development/development/app/web/.htaccess

    cleanUpRemoteProject

    moveFileadmin

    setSymlink

    cleanUpLocalProject

    cloneGitRepos

    initOtherStates

    finishScript
}

deploy() {
    clear
    infoMsg "$DRY"

    #TODO:
        infoMsg "::\n" \
        ":: Script not finished\n" \
        "::\n"
        exitMsg
        exit

    showVariables

    read -rsp $'\n\nPress any key to continue...\n' -n1 key

    #TODO: check if ini�s exists
    checkPlaceholderVariables

    infoMsg "::\n" \
    ":: update development TYPO3\n" \
    "::\n"

    #TODO:
    #backupRemoteDevelopment

    #TODO Handle fildeadmin true/false
    uploadLocalProjectToDevelopment

    #TODO:
    #handleDatabasesForDeploy

    setPermissions

    #TODO: check .htaccess
    logMsg "TODO: check .htaccess"
    #logMsg "rename .htaccess"
    #ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/_.htaccess ${remoteFolder}/releases/development/development/app/web/.htaccess

    cleanUpRemoteProject

    #TODO: check if already exists
    setSymlink

    cleanUpLocalProject

    #TODO:
    #check if hive_installer and hive_deploy exist
    #clone or:
    #fetch && merge repos

    finishScript
}

showVariables() {
    infoMsg "::\n" \
    ":: Version: $version\n" \
    "::\n" \
    ":: Variables:\n" \
    ":: boilerplatename: $boilerplatename\n" \
    ":: sshuser: $sshuser\n" \
    ":: remotefolderpath: $remotefolderpath\n" \
    ":: repoinstaller: $repoInstaller\n" \
    ":: repodeploy: $repoDeploy\n" \
    "::\n" \
    ":: development_database: $development_database\n" \
    ":: development_host: $development_host\n" \
    ":: development_password: $development_password\n" \
    ":: development_port: $development_port\n" \
    ":: development_username: $development_username\n" \
    "::\n" \
    ":: staging_database: $staging_database\n" \
    ":: staging_host: $staging_host\n" \
    ":: staging_password: $staging_password\n" \
    ":: staging_port: $staging_port\n" \
    ":: staging_username: $staging_username\n" \
    "::\n" \
    ":: production_database: $production1_database\n" \
    ":: production_host: $production1_host\n" \
    ":: production_password: $production1_password\n" \
    ":: production_port: $production1_port\n" \
    ":: production_username: $production1_username\n" \
    "::\n" \
    ":: production1_database: $production2_database\n" \
    ":: production1_host: $production2_host\n" \
    ":: production1_password: $production2_password\n" \
    ":: production1_port: $production2_port\n" \
    ":: production1_username: $production2_username\n" \
    "::\n" \
    ":: hotfix_database: $hotfix_database\n" \
    ":: hotfix_host: $hotfix_host\n" \
    ":: hotfix_password: $hotfix_password\n" \
    ":: hotfix_port: $hotfix_port\n" \
    ":: hotfix_username: $hotfix_username\n" \
    "::\n"
}

checkPlaceholderVariables() {
    #check placeholder variables
    if [ "$sshUser" == "xxx@xxx.xx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \sshuser must not be '$sshuser'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$remoteFolder" == "xxx/xxx/" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \remotefolderpath must not be '$remotefolderpath'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$boilerplatename" == "hive_boilerplate_YY-mm-dd_HH-MM-SS" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \boilerplatename must not be '$boilerplatename'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$development_database" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \development_database must not be '$development_database'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$development_password" == "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \development_password must not be '$development_password'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$development_username" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \development_username must not be '$development_username'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$staging_database" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \staging_database must not be '$staging_database'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$staging_password" == "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \staging_password must not be '$staging_password'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$staging_username" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \staging_username must not be '$staging_username'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production1_database" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production1_database must not be '$production1_database'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production1_password" == "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production1_password must not be '$production1_password'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production1_username" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production1_username must not be '$production1_username'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production2_database" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production2_database must not be '$production2_database'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production2_password" == "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production2_password must not be '$production2_password'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$production2_username" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \production2_username must not be '$production2_username'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$hotfix_database" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \hotfix_database must not be '$hotfix_database'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$hotfix_password" == "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \hotfix_password must not be '$hotfix_password'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if [ "$hotfix_username" == "xxxxxxxxxx" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \hotfix_username must not be '$hotfix_username'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    #check if docker-compose.yml exists
    if [ ! -f "../${boilerplatename}/docker-compose.yml" ]; then
       errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <../${boilerplatename}/docker-compose.yml> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    #check if docker-sync.yml doesn't exists
    if [ -f "../${boilerplatename}/docker-sync.yml" ]; then
        errorMsg ":: Error [fsf9qxy3i3pi4k4usi18umx21r9q837v] in configuration\n" \
        ":: File <../${boilerplatename}/docker-sync.yml> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    #check hive_installer_typo3 placeholder
    if [ "$repoInstaller" == "hive_installer_typo3" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repoinstaller must not be '$repoInstaller'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    #check hive_deploy placeholder
    if [ "$repoDeploy" == "hive_deploy_typo3" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repodeploy must not be '$repoDeploy'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    #check placeholder variables
    if [ "$repoThm" == "XXX_YY_xxx_deploy" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repodeploy must not be '$repoDeploy'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
    fi
}

setupRemoteFolders() {
    ###### setup remote folders ######
    logMsg "setup remote folders\n"

    if ssh $sshUser [ ! -d "${remoteFolder}" ]; then
        errorMsg ":: Error [kk6vegv83o41kki30nzexyd6y6w5wuwy] in configuration\n" \
        ":: Folder <${remoteFolder}> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ -d "${remoteFolder}/fileadmin" ]; then
        errorMsg ":: Error [qxzgtq3sx11t66j65go6v7hrp97du61q] in configuration\n" \
        ":: Folder <fileadmin> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/config" ]; then
        logMsg "create ../config/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/config
    else
        errorMsg ":: Error [qxzgtq3sx11t66j65go6v7hrp97du61q] in configuration\n" \
        ":: Folder <config> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/trash" ]; then
        logMsg "create ../trash/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/trash
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/trash/${trashTime}" ]; then
        logMsg "create ../trash/${trashTime} remote directory"
        ssh $sshUser mkdir ${remoteFolder}/trash/${trashTime}
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/config/deploy" ]; then
        logMsg "create ../config/deploy/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/config/deploy
    fi

     if ssh $sshUser [ ! -d "${remoteFolder}/releases/" ]; then
        logMsg "create ../releases/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases
    else
        errorMsg ":: Error [qxzgtq3sx11t66j65go6v7hrp97du61q] in configuration\n" \
        ":: Folder <releases> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/development/" ]; then
        logMsg "create ../releases/development/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/development
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/development/development/" ]; then
        logMsg "create ../releases/development/development/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/development/development/
    fi

#    if ssh $sshUser [ ! -d "${remoteFolder}/releases/development/development/sqldump" ]; then
#        logMsg "create ../releases/development/development/sqldump/ remote directory"
#        ssh $sshUser mkdir ${remoteFolder}/releases/development/development/sqldump
#    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/development/developmentapp" ]; then
        logMsg "create ../releases/development/development/app/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/development/development/app
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/staging/" ]; then
        logMsg "create ../releases/staging/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/staging
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/staging.hotfix/" ]; then
        logMsg "create ../releases/staging.hotfix/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/staging.hotfix
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/releases/production/" ]; then
        logMsg "create ../releases/production/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/releases/production
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/lib/" ]; then
        logMsg "create ../lib/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/lib
    else
        errorMsg ":: Error [qxzgtq3sx11t66j65go6v7hrp97du61q] in configuration\n" \
        ":: Folder <lib> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/lib/teufels/" ]; then
        logMsg "create ../lib/teufels/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/lib/teufels
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/lib/teufels/tasks/" ]; then
        logMsg "create ../lib/teufels/tasks/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/lib/teufels/tasks
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/migrations/" ]; then
        logMsg "create ../migrations/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/migrations
    else
        errorMsg ":: Error [qxzgtq3sx11t66j65go6v7hrp97du61q] in configuration\n" \
        ":: Folder <migrations> does exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        if [ "$DRY" = "dry" ]; then
            warningMsg "DRY RUN";
        else
            exitMsg
            exit
        fi
    fi

    if ssh $sshUser [ ! -d "${remoteFolder}/migrations/db/" ]; then
        logMsg "create ../migrations/db/ remote directory"
        ssh $sshUser mkdir ${remoteFolder}/migrations/db
    fi
}

uploadConfigDatabases() {
    ###### upload config database files ######
    logMsg "upload config"
    runMsg "cd src"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        cd src
    fi

    logMsg "compress config"
    runMsg "tar -pczf config.tar.gz development.ini staging.ini production1.ini production.ini hotfix.ini"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        tar -pczf config.tar.gz development.ini staging.ini production1.ini production.ini hotfix.ini
        sleep 1
    fi

    logMsg "upload config.tar.gz"
    runMsg "scp -p config.tar.gz ${sshUser}:${remoteFolder}/config/deploy"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        scp -p config.tar.gz ${sshUser}:${remoteFolder}/config/deploy
        sleep 1
    fi

    logMsg "extract config.tar.gz"
    runMsg "ssh ${sshUser} tar -pxzf ${remoteFolder}/config.tar.gz -C ${remoteFolder}"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} tar -pxzf ${remoteFolder}/config/deploy/config.tar.gz -C ${remoteFolder}/config/deploy
        sleep 2
    fi

#    logMsg "rename a.ini to development.ini"
#    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/config/deploy/a.ini ${remoteFolder}/config/deploy/development.ini"
#    if [ "$DRY" = "dry" ]; then
#        warningMsg "DRY RUN";
#    else
#        ssh ${sshUser} mv -v ${remoteFolder}/config/deploy/a.ini ${remoteFolder}/config/deploy/development.ini
#    fi

    #clean up
    logMsg "clean up .."
    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/config/deploy/config.tar.gz ${remoteFolder}/trash/${trashTime}/config.tar.gz"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} mv -v ${remoteFolder}/config/deploy/config.tar.gz ${remoteFolder}/trash/${trashTime}/config.tar.gz
    fi

    logMsg "clean up local config.tar.gz"
    runMsg "rm -rf config.tar.gz"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        rm -rf config.tar.gz
    fi

    runMsg "cd .."
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        cd ..
    fi
}

exportLocalDatabaseToDevelopment() {
    ###### export/compress/upload/extract/import local database ######
    time=$(date +"%Y-%m-%d_%H-%M-%S")
    logMsg "start export/compress/upload/extract/import local database"
    runMsg "cd ../${boilerplatename}"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        cd ../${boilerplatename}
    fi

    #check if files already exist
    if [ -f "backup/mysql.sql" ]; then
        logMsg "rename existing backup/mysql.sql file to backup/mysql_${time}.sql"
        mv -v "backup/mysql.sql" "backup/mysql_${time}.sql"
    fi

    if [ -f "backup/mysql.tar.gz" ]; then
        logMsg "rename existing backup/mysql.tar.gz file to backup/mysql_${time}.tar.gz "
        mv -v"backup/mysql.tar.gz" "backup/mysql_${time}.tar.gz "
    fi

    #stop all running docker containers
    logMsg "stopping all running docker containers"
    runMsg "docker stop $^(docker ps -a -q^)"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        docker stop $(docker ps -a -q)
        sleep 2
    fi

    #start docker
    logMsg "start docker containers"
    runMsg "docker-compose up -d"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        docker-compose up -d
        sleep 12
    fi

    logMsg "export local database"
    runMsg "make -f Makefile.teufels mysql-export"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        make -f Makefile.teufels mysql-export
        sleep 3
    fi

    logMsg "compress mysql.sql to mysql.tar.gz"
    runMsg "cd backup"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        cd backup
    fi
    runMsg "tar -pczf mysql.tar.gz mysql.sql"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        tar -pczf mysql.tar.gz mysql.sql
    fi

    #upload database
    logMsg "upload mysql.sql"
    runMsg "scp -p mysql.tar.gz ${sshUser}:${remoteFolder}/migrations/db"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        scp -p mysql.tar.gz ${sshUser}:${remoteFolder}/migrations/db
        sleep 2
    fi

    runMsg "cd .."
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        cd ..
    fi

    #export database
    logMsg "extract mysql.tar.gz"
    runMsg "ssh ${sshUser} tar -pxzf ${remoteFolder}/migrations/db/mysql.tar.gz -C ${remoteFolder}/migrations/db"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} tar -pxzf ${remoteFolder}/migrations/db/mysql.tar.gz -C ${remoteFolder}/migrations/db
        sleep 2
    fi

    #import database
    logMsg "import mysql.sql"
    runMsg "ssh ${sshUser} mysql -h$development_host $development_database -u$development_username -p$development_password --default-character-set=utf8 \< ${remoteFolder}/migrations/db/mysql.sql"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} mysql -h$development_host $development_database -u$development_username -p$development_password --default-character-set=utf8 \< ${remoteFolder}/migrations/db/mysql.sql
        sleep 2
    fi

    #clean up folder
    logMsg "clean up ../migrations/db/"
    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/migrations/db/mysql.tar.gz ${remoteFolder}/trash/${trashTime}/mysql.tar.gz"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} mv -v ${remoteFolder}/migrations/db/mysql.tar.gz ${remoteFolder}/trash/${trashTime}/mysql.tar.gz
    fi
    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/migrations/db/mysql.sql ${remoteFolder}/trash/${trashTime}/mysql.sql"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} mv -v ${remoteFolder}/migrations/db/mysql.sql ${remoteFolder}/trash/${trashTime}/mysql.sql
    fi
}

uploadLocalProjectToDevelopment() {
    ###### cleartemp/compress/upload/extract local project ######
    time=$(date +"%Y-%m-%d_%H-%M-%S")
    logMsg "start cleartemp/compress/upload/extract local project"

    #clear local cache
    if [ "$(ls -A app/web/typo3temp)" ]; then
       logMsg "clear local typo3temp folder"
       rm -rf "/app/web/typo3temp/*"
    fi

    #compress local project
    runMsg "cd app/"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       cd app/
    fi
	runMsg "tar -pczf project.tar.gz vendor/* web/*"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       tar -pczf project.tar.gz vendor/* web/*
    fi
	runMsg "cd .."
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       cd ..
    fi

    # upload local project
    logMsg "upload compressed project"
    runMsg "scp -p app/project.tar.gz ${sshUser}:${remoteFolder}/releases/development/development"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       scp -p app/project.tar.gz ${sshUser}:${remoteFolder}/releases/development/development
    fi

	# extract project
	runMsg "ssh ${sshUser} tar -pxzf ${remoteFolder}/releases/development/development/project.tar.gz -C ${remoteFolder}/releases/development/development/app"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} tar -pxzf ${remoteFolder}/releases/development/development/project.tar.gz -C ${remoteFolder}/releases/development/development/app
       sleep 1
    fi
}

#handleDatabaseForInit() {
#    logMsg "remove old db.ini"
#    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/typo3conf/db.ini ${remoteFolder}/trash/${trashTime}/db.ini"
#    if [ "$DRY" = "dry" ]; then
#        warningMsg "DRY RUN";
#    else
#       ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/typo3conf/db.ini ${remoteFolder}/trash/${trashTime}/db.ini
#    fi
#
#    logMsg "copy development.ini to project"
#    runMsg "ssh ${sshUser} cp -v ${remoteFolder}/config/deploy/development.ini ${remoteFolder}/releases/development/development/app/web/typo3conf/db.ini"
#    if [ "$DRY" = "dry" ]; then
#        warningMsg "DRY RUN";
#    else
#       ssh ${sshUser} cp -v ${remoteFolder}/config/deploy/development.ini ${remoteFolder}/releases/development/development/app/web/typo3conf/db.ini
#    fi
#}

setPermissions() {
    logMsg "set permissions for web and vendor folder"
    runMsg "ssh ${sshUser} chmod -R 755 ${remoteFolder}/releases/development/development/app/web"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} chmod -R 755 ${remoteFolder}/releases/development/development/app/web
    fi
    runMsg "ssh ${sshUser} chmod -R 755 ${remoteFolder}/releases/development/development/app/vendor"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} chmod -R 755 ${remoteFolder}/releases/development/development/app/vendor
    fi
}

cleanUpRemoteProject() {
    #clean up folder
    logMsg "clean up ../releases/development/development/"
	runMsg "ssh  ${sshUser} mv -v ${remoteFolder}/releases/development/development/project.tar.gz ${remoteFolder}/${trashTime}/project.tar.gz"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh  ${sshUser} mv -v ${remoteFolder}/releases/development/development/project.tar.gz ${remoteFolder}/trash/${trashTime}/project.tar.gz
    fi
}

moveFileadmin() {
    logMsg "move fileadmin"
    runMsg "ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/fileadmin ${remoteFolder}/fileadmin"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development/app/web/fileadmin ${remoteFolder}/fileadmin
    fi
}

setSymlink() {
    #set symlinks
    logMsg "Set symlink for development"
    runMsg "ssh ${sshUser} ln -s releases/development/development/app/web/ ${remoteFolder}/development"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} ln -s releases/development/development/app/web/ ${remoteFolder}/development
    fi

    logMsg "Set symlink for databases in typo3conf"
    runMsg "ssh ${sshUser} ln -s ../../../../../../config/deploy ${remoteFolder}/releases/development/development/app/web/typo3conf/config"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} ln -s ../../../../../../config/deploy ${remoteFolder}/releases/development/development/app/web/typo3conf/config
    fi

    logMsg "Set symlink for fileadmin in development"
    runMsg "ssh ${sshUser} ln -s ../../../../../fileadmin ${remoteFolder}/releases/development/development/app/web/fileadmin"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       ssh ${sshUser} ln -s ../../../../../fileadmin ${remoteFolder}/releases/development/development/app/web/fileadmin
    fi
}

cleanUpLocalProject() {
    # remove local project.tar.gz
    logMsg "remove local project.tar.gz"
    runMsg "rm -rf app/project.tar.gz"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
       rm -rf app/project.tar.gz
    fi
}

cloneGitRepos() {
    #add hive_installer to project
    logMsg "clone hive_installer to remote project folder"
    runMsg "git clone git@bitbucket.org:teufels/${repoInstaller}.git ${remoteFolder}/hive_installer"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} git clone git@bitbucket.org:teufels/${repoInstaller}.git ${remoteFolder}/hive_installer && runMsg "hive_install cloned" || errorMsg "hive_install clone failed"
    fi

    #add hive_deploy to project
    logMsg "clone hive_deploy to remote project folder"
    runMsg "git clone git@bitbucket.org:teufels/${repoDeploy}.git ${remoteFolder}/hive_deploy"
    if [ "$DRY" = "dry" ]; then
        warningMsg "DRY RUN";
    else
        ssh ${sshUser} git clone git@bitbucket.org:teufels/${repoDeploy}.git ${remoteFolder}/hive_deploy && runMsg "hive_deploy cloned" || errorMsg "hive_deploy clone failed"
    fi
}

initOtherStates() {
    logMsg "initialize other states"
    logMsg "copy development to staging"
    ssh ${sshUser} mv -v ${remoteFolder}/releases/development/development ${remoteFolder}/releases/staging/staging
    sleep 2

    logMsg "copy staging to production"
    ssh ${sshUser} mv -v ${remoteFolder}/releases/staging/staging ${remoteFolder}/releases/production/production
    sleep 1
}

finishScript() {
    infoMsg "script finished"
    endCounter=$(date +%s)
    totalTime=$(echo "$endCounter - $startCounter" | bc)
    echo " total time:" $(convertsecs $totalTime)
}

convertsecs() {
    h=`expr $1 / 3600`
    m=`expr $1  % 3600 / 60`
    s=`expr $1 % 60`
    printf "%02d:%02d:%02d\n" $h $m $s
}
errorMsg() {
    echo -e "[$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]\n" \
    $(tput setaf 1)$(tput setab 0)"\n"\
    $(tput setaf 1)$(tput setab 0)$*$(tput sgr 0)
}

checkMsg() {
    echo -e "" \
    "$(tput setaf 2)[x] $*$(tput sgr 0)"
}
checkWarningMsg() {
    echo -e "" \
    "$(tput setaf 3)[!] $*$(tput sgr 0)"
}
checkNotMsg() {
    echo -e $(tput setaf 1)$(tput setab 0)"" \
    "$(tput setaf 1)$(tput setab 0)[-] $*$(tput sgr 0)"
}

runMsg() {
    echo -e "" \
    "[run] '$*' [$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]"
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "\n" \
    "$*"
}

exitMsg() {
    errorMsg "[EXIT]\n" \
    "Good bye'"
}

warningMsg() {
    echo -e "" \
    "$(tput setaf 3)$(tput setab 0)$*$(tput sgr 0)"
}

infoMsg "Hello!"

DRY=""

if [ "$1" = "init" ]; then
    init
else
    if [ "$1" = "dry" ]; then
        DRY="$1"
        deploy
    else
        deploy
    fi
fi